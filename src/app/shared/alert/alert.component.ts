import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from "../../service";
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  message: any;
  constructor(public alertService : AlertService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.subscription = this.alertService.getMessage().subscribe(message => {
      this.message = message ? message.text : '';
      if(this.message){
        this.openSnackBar(this.message)
      }
      setTimeout(() => {
        this.message = '';
      }, 4000);
    });
  }

  openSnackBar(message) {
    this._snackBar.open(message, '', {
      duration: 3000
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
