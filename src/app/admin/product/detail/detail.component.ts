import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AdminService } from "../.../../../../service";
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public routeObj: any;
  productID: any;
  public productDetail : any;
  constructor(public route : ActivatedRoute, public adminService : AdminService, public router : Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.routeObj = { ...params.keys, ...params };
      this.productID = this.routeObj.params.id;
      console.log(params, 'this.productID')
      if(this.productID){
        this.getProductDetail(this.productID)
      }
    });
  }

  getProductDetail(id){
    this.adminService.getProductDetail(id).subscribe(data => {
      this.productDetail = data['data']
    })
  }

  viewProduct(id){
    this.router.navigate([`/product/detail/${id}`])
  }

}
