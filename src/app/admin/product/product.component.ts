import { Component, OnInit } from '@angular/core';
import { AdminService, AuthService } from "../../service";
import { Router, ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  public products: Array<any> = [];
  public categories: Array<any> = [];
  totalCount: number;
  pageIndex: number = 0;
  pageSize: number;
  length: number;
  timeout = null;
  selectedCategory: string = '';
  public dataLoader: boolean = true;
  productTitle: string;
  routeObj
  menuId: string;
  public myProductArray: Array<any> = []
  public searchData: object = {
    limit: 9,
    offset: 0,
    menu: ''
  }
  public allData: object = {
    limit: 1000,
    offset: 0,
    menuId: ''
  }
  getProductData: Subscription;
  currentUser: any;
  constructor(public adminService: AdminService, public authService: AuthService, public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUserValue.data
    this.activatedRoute.queryParamMap.subscribe(params => {
      this.routeObj = { ...params.keys, ...params };
      this.menuId = this.routeObj.params.id;
      if (this.menuId) {
        this.pageIndex = this.routeObj.params.index ? this.routeObj.params.index : 0
        this.searchData['offset'] = this.routeObj.params.index ? this.routeObj.params.index * 9 : 0;
        this.searchData['menu'] = this.menuId
        this.allData['menuId'] = this.menuId
        this.getProducts(this.searchData)
        this.getCategory()
      }
    });
    // this.getProductData = this.adminService.getProductData$.subscribe(data => {
    //   this.searchData['menu'] = data;
    //   this.getProducts(this.searchData);
    // })

    this.getProducts(this.searchData);
  }

  getProducts(searchData) {
    this.dataLoader = true;
    this.adminService.getProducts(searchData).subscribe(data => {
      this.products = data['data']
      this.totalCount = data['total']
      this.dataLoader = false;
    }, error => {
      this.dataLoader = false;
    })
  }

  getCategory() {
    this.adminService.getCategories(this.allData).subscribe(data => {
      this.categories = data['data']
    }, error => {

    })
  }

  goToAddProduct() {
    this.router.navigate(['/product/add'])
  }

  editProduct(id) {
    this.router.navigate(['/product/add'], { queryParams: { id: id, menuId: this.menuId, index : this.pageIndex } })
  }

  viewProduct(id) {
    this.router.navigate([`/product/detail/${id}`])
  }
  deleteProduct(id) {
    let result = prompt("Please enter DELETE to delete");
    if (result == 'DELETE') {
      this.adminService.deleteProduct(id).subscribe(data => {
        this.getProducts(this.searchData);
      }, error => {

      })
    }
  }

  getServerData(event) {
    document.getElementsByTagName('mat-sidenav-content')[0].scrollTo(0, 0)
    const { pageSize, pageIndex } = event;
    let limit = pageSize;
    if (pageIndex === this.pageIndex + 1) {
      this.searchData['offset'] = this.searchData['offset'] + pageSize
    } else if (pageIndex === this.pageIndex - 1) {
      this.searchData['offset'] = this.searchData['offset'] - pageSize
    }
    this.pageIndex = pageIndex;
    this.searchData['limit'] = limit
    this.getProducts(this.searchData)
  }

  searchByTitle(event) {
    this.pageIndex = 0;
    this.searchData['title'] = event.target.value
    this.searchData['offset'] = 0
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      this.getProducts(this.searchData)
    }, 1000);
  }

  filterProductByCategory(event) {
    this.pageIndex = 0;
    this.searchData['offset'] = 0
    this.searchData['category'] = event.value
    this.getProducts(this.searchData)
  }

  selectProduct(item) {
    let opts = {
      product : item._id,
      category : item.category,
      subCategory : item.subCategory,
      title : item.title,
      price : item.price,
      regularPrice : item.regularPrice,
      keywords : item.keywords,
      quantity : item.quantity,
      productCode : item.productCode,
      brand : item.brand,
      isRented : item.isRented ? item.isRented : false,
      isSameDay : item.isSameDay ? item.isSameDay : false
    }
    this.adminService.addMyProduct(opts).subscribe(data => {
      this.getProducts(this.searchData)
    }, error => {

    })
  }

}
