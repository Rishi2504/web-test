import { Component, OnInit } from '@angular/core';
import { AdminService } from "../../service";
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: Array<any> = []
  usersColumns: Array<any> = ['firstName', 'lastName', 'email']
  constructor(public adminService : AdminService) { }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers() {
    this.adminService.getUsers().subscribe(data => {
      this.users = data['data'];
    })
  }

}
